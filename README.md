# Lab 4 - An Introduction to Smart Pointers

See the Lab 4 handout for the details on how to complete this lab.

## Generating Projects

As usual, this repo contains a number of `cmake` generator options. For example, navigate to the `build/unix` folder and run `cmake` to generate Unix make files that can be used to easily create executables from the source code.

```bash
$ cd build/unix
$ cmake -G "Unix Makefiles" ../..
... lots of output ...
$ ./Lab04
... lab executable output ...
$
```
